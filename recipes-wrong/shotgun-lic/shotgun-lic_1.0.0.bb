SUMMARY = "Shotgun License"
DESCRIPTION = "I do not understand DEPENDS or RDEPENDS"
LICENSE = "LGPL-2.1 & MyWeirdProprietaryLicense"
SRC_URI = "file://shotgun-lic-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://COPYING;md5=1a6d268fd218675ffea8be556788b780 \
                    file://MyWeirdProprietaryLicense;md5=f9bef5af8a33f08319292014b73d671b \
                    "

ALLOW_EMPTY_${PN} = "1"
