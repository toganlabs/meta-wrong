SUMMARY = "Closed App using hello library"
LICENSE = "CLOSED"
SRC_URI = "file://use-hello-lib-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://app.py;endline=5;md5=53756c8912a0b93e1a2bac46c50e2850"

DEPENDS = "hello-lib"

ALLOW_EMPTY_${PN} = "1"
