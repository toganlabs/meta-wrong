SUMMARY = "LGPL Library"
LICENSE = "LGPLv2"
SRC_URI = "file://hello-lib-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://hello_lib.py;endline=16;md5=13d9d5f0ea0eda50a136656fc4d71d7e"

ALLOW_EMPTY_${PN} = "1"
