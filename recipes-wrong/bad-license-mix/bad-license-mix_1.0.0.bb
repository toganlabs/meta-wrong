SUMMARY = "Bad license mix"
LICENSE = "CLOSED & GPLv2"
SRC_URI = "file://bad-license-mix-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://app.py;endline=16;md5=13d9d5f0ea0eda50a136656fc4d71d7e"

ALLOW_EMPTY_${PN} = "1"
