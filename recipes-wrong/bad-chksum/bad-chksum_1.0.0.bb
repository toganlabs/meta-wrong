SUMMARY = "Bad Chksum"
LICENSE = "GPL-2"
SRC_URI = "file://bad-chksum-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://app.py;endline=16;md5=53756c8912a0b93e1a2bac46c50e2850"

ALLOW_EMPTY_${PN} = "1"
