SUMMARY = "MIT licensed App"
LICENSE = "MIT"
SRC_URI = "file://mit-app-1.0.0.tar.gz"
LIC_FILES_CHKSUM = "file://app.py;endline=16;md5=f4b5e7509f14d42caed1741effd69c40"

ALLOW_EMPTY_${PN} = "1"
