DESCRIPTION = "An image with license issues" 

#" lgpl-library mit-app use-library"

IMAGE_INSTALL = "\
    bad-chksum bad-license-mix closed-app mit-app hello-lib use-hello-lib \
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    "

inherit core-image
